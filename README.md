# COVID-19: age-structured transmission model for Switzerland

This repository contains R scripts implementing an age-structured transmission model similar to the one described in Di Domenico et al. 2020, but adapted to the COVID-19 situation in Switzerland.

**The details of the model and its implementation are described in *covid_model_full_description.pdf*.**

The data used to calibrate the model and estimate the model parameters are available in the folder *data*.

Result plots shown in the README relates to our model applied to Switzerland as a whole.

A specific version of the model for the Geneva canton only can be found in the *geneva* directory, with R scripts to fit the model's free parameters and run the model. Similar canton-specific models are also available for Bern and Ticino.

For comparison purposes, a description of existing models for COVID-19 in Switzerland, with their structure, purpose and parameters, can be found in *covid19_switzerland_models_comparison.pdf*.

The folder *former_results* contains PDFs exposing preliminary results from a previous version of the model.

For any question on this model, please contact janne.estil@unige.ch or olivia.keiser@unige.ch


# Important
The aim of this repository is to propose an example of transmission model for COVID-19, as an implementation test case, with its strengths and limitations.
The output depends on model assumptions and parameter choices. As such, produced results should be interpreted carefully and responsibly.
It should be used for informational and research purposes only. Authors are not liable for any direct or indirect consequences of this usage.


# Some results
Fit of deaths and hospitalized patients compared with the data
![](plots_for_readme/Fitting.png)

Results of the main analysis from different scenarios:
a) Reproductive number, b) daily COVID-19 related deaths, c) daily intensive care unit (ICU) bed occupancy, and d) cumulative infections in Switzerland from 11 February to 31 December 2020.
“Strong reduction” refers to a 56% reduction of contacts (calibrated to prevent ICU overflow with 90% probability), “light reduction” is half of that, and “minimizing contacts” refers to 95% reduction of contacts.
![](plots_for_readme/MainAnalysis.png)

Similar results for the sensitivity analysis with seasonal forcing
![](plots_for_readme/SensitivityAnalysis_Seasonality.png)



# Other activities of our group around COVID-19
- In collaboration with the Federal Office of Public Health and many hospitals in Switzerland we developed a sentinel system to capture detailed clinical information of hospitalized patients in Switzerland. More details can be found here: https://www.unige.ch/medecine/hospital-covid/
- Participation in the national taskforce  https://ncs-tf.ch/en/. Antoine Flahault (Public health group); Olivia Keiser (group on data and modeling)
- We are working on a detailed network based model for COVID-19 transmission in Switzerland (ongoing).
- We are further developing our semi-automated systematic review software (https://www.medrxiv.org/content/10.1101/2020.03.12.20034702v1) to quickly classify scientific articles on covid-19 or other topics of interest into sub-topics (ongoing).


# Team members contributing to all these activities (alphabetical order):
- Alexander Temerev (text mining, contributes to network model)
- Amaury Thiabaud (hospital surveillance)
- Barbara Bertisch (general support, clinical care)
- Erol Orel (contributes to text mining, mathematical model, hospital surveillance, website)
- Isotta Triulzi (volunteer and former visiting student, general support)
- Janne Estill (mathematical model)
- Janos Nadaban (volunteer, general support)
- Liudmila Rozanova (volunteer, network model)
- Maroussia Roelens (hospital surveillance, website)
- Olivia Keiser (supervision)
- Plamenna Venkova (hospital surveillance, mathematical model, website)
- Rachel Esra (general support)


If you are interested to contribute to our activities or if you want to get in touch, then please contact olivia.keiser@unige.ch
Link to our group website: https://www.unige.ch/medecine/isg/en/research/988keiser/

# Other activities of the Institute of Global Health around COVID-19
- COVID-19 Epidemic Forecasting (https://renkulab.shinyapps.io/COVID-19-Epidemic-Forecasting/). Authors: Antoine Flahault (Antoine.Flahault@unige.ch), Christine Choirat (christine.choirat@datascience.ch), Elisa Manetti (Elisa.Manetti@etu.unige.ch)
