

canton <- "Total"
cases <- read.xlsx("covid_19_data_switzerland 110520.xlsx", sheetName="Cases")
cases_dates <- as.Date(cases[,1])
data_cases <- c(0, diff(cases[,28]))
deaths <- read.xlsx("covid_19_data_switzerland 110520.xlsx", sheetName="Fatalities")
deaths_date <- as.Date(deaths[,1])
data_deaths <- c(0, diff(deaths[,28]))
inhosp <- read.xlsx("covid_19_data_switzerland 110520.xlsx", sheetName="Hospitalized")
inhosp_date <- as.Date(inhosp[,1])
data_inhosp <- inhosp[,28]
inICU <- read.xlsx("covid_19_data_switzerland 110520.xlsx", sheetName="ICU")
inICU_date <- as.Date(inICU[,1])
data_inICU <- inICU[,28]






n_simuls <- 1000
max_days <- 400

epsilon <- 1/3.7 # E to Ip
mu_p <- 1/1.5 # Ip to Ia/Ips/Ims/Iss
mu <- 1/2.3 # Ia/Ips/Ims to recovery; Iss to hospitalization (assumed to be equal)
p_aC <- 0.5 # Probability to be asymptomatic: children
p_aA <- 0.5 # Probability to be asymptomatic: adults
p_aS <- 0.5 # Probability to be asymptomatic: seniors
p_msC <- 0 # If symptomatic, probability to have mild symptoms (but excluding paucisymptomatic): children
p_msA <-0.7 # ...
p_msS <- 0.6 # ...
p_ssC <- 0 # If symptomatic, probability to have severe symptoms: children
p_ssA <- 0.03 #0.1 #...
p_ssS <- 0.35 #0.2 # ...
p_ICUC <- 0 # If hospitalized, probability to go to ICU: children
p_ICUA <- 0.25 #0.36 # ...
p_ICUS <- 0.2 #0.2 # ...
lambdaHRC <- 0 # H to R: children
lambdaHRA <- 0.072 # ...
lambdaHRS <- 0.022 # ...
lambdaHDC <- 0
#lambdaHDA <- 2*0.0042 # ...
#lambdaHDS <- 4*0.014 # ...
lambdaICURC <- 0
lambdaICURA <- 0.05 # ...
lambdaICURS <- 0.036 # ...
lambdaICUDC <- 0
#lambdaICUDA <- 2*0.0074 # ...
#lambdaICUDS <- 4*0.029 # ...
rb <- 0.51 # relative infectiousness of prodomic, asymptomatic and paucisymptomatic individuals


#beta0 <- 0.8 # 0.74

relCC <- 2
relCA <- 0.5
relCS <- 0.2
relAS <- 0.4
relSS <- 1

#schoolCC <- 0.05
#schoolCA <- 0.8
#workAA <- 0.5
#shopAA <- 0.5 #0.5
#social1 <- 0.75
#social2 <- 0.75*0.65
restaurantAA <- 0.8




lambdaHDA <- 0.009324011 #0.009483796
lambdaHDS <- 0.02971054 #0.02826953
lambdaICUDA <- 0.01552885 #0.016140518
lambdaICUDS <- 0.3501639 #0.27265618
start_date_fixed <- as.Date("2020/02/11") #as.Date("2020/02/12") #as.Date("2020/02/08")
beta0 <- 0.7931006 #0.8106221 #0.8945111
schoolCC <- 0.0784603 #0.04674538 #U0.05488696
schoolCA <- 0.5132853 #0.5928436 #0.9311092
workAA <- 0.2363408 #0.3572992 #0.2116836
shopAA <- 0.2956368 #0.2849987 #0.3633391 #0.3898732
social1 <- 0.88 #0.8785896 #0.9421520 #0.8950454
social2 <- 0.53 #0.5555012 #0.5 #0.5603637 #0.5192640
inseed <- 49 #50 #4

#tracA <- 0.1
#tracing <- c(1,1,1,0.44,0.44,0.44)
#tracing <- c(0.72,0.44,0.72,0.44,0.44,0.72)
#tracS <- 1 #0.1
#tracing <- c(1,1,tracS,1,tracS,tracS^2)
#tracing <- 0.44*c(1,1,1,1,1,1)
#tracing <- c(0.72,0.72,0.05,0.72, 0.05, 0.72)
#tracing <- c(1,1,1,1,1,1)
daily_hosps <- matrix(nrow=n_simuls, ncol=max_days)

daily_deaths <- matrix(nrow=n_simuls, ncol=max_days)

daily_infections <- matrix(nrow=n_simuls, ncol=max_days)

cumul_infections <- matrix(nrow=n_simuls, ncol=max_days)

daily_inhospital <- matrix(nrow=n_simuls, ncol=max_days)

daily_inICU <- matrix(nrow=n_simuls, ncol=max_days)

Repr <- matrix(nrow=n_simuls, ncol=max_days)

canton <- "Total"
modelsolution <- "combined"


for (i in 1:n_simuls) {
  source("model_GE.R")
  daily_hosps[i,] <- newhosps_C+newhosps_A+newhosps_S
  daily_deaths[i,] <- newdeaths_C+newdeaths_A+newdeaths_S
  daily_infections[i,] <- newcases_C+newcases_A+newcases_S
  cumul_infections[i,] <- cumsum(daily_infections[i,])
  daily_inhospital[i,] <- HC+HA+HS+ICUC+ICUA+ICUS
  daily_inICU[i,] <- ICUC+ICUA+ICUS
  Repr[i,] <- Re
  print(i)
}

Repr[which(is.na(Repr))] <- 0
for (i in (4:(ncol(Repr)-3))) {
  Repr[,i] <- apply(Repr[,((i-3):(i+3))], 1, sum)/7
}
for (i in (1:3)) {
  Repr[,i] <- Repr[,4]
}
for (i in ((ncol(Repr)-2):ncol(Repr))) {
  Repr[,i] <- Repr[,(ncol(Repr)-3)]
}


plot(x=seq(from=as.Date("2020/02/11"), to=as.Date("2020/05/10"), by=1), y=rep(10000, length=length(seq(from=as.Date("2020/02/11"), to=as.Date("2020/05/10"), by=1))),  ylim=c(0,1000))

#plot(x=seq(from=as.Date("2020/02/11"), to=as.Date("2020/12/31"), by=1), y=rep(10000, length=length(seq(from=as.Date("2020/02/11"), to=as.Date("2020/12/31"), by=1))),  ylim=c(0,max(max(daily_hosps),data_hosps)))
#plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/05/01"), by=1), y=rep(2000, length=74),  ylim=c(0,max(max(daily_hosps[,(1:75)]),data_hosps)))
for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/11"), to=as.Date("2020/02/11")+max_days-1, by=1), y=daily_hosps[i,], col="green", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/11"), to=as.Date("2020/02/11")+max_days-1, by=1), y=apply(daily_hosps,2,sum)/n_simuls, col="darkgreen", lwd=2)
#lines(x=casedeath_dates, y=data_hosps, col="gray", lwd=2, lty="dashed")
png(file="")

plot(x=deaths_date, y=data_deaths, xlim=c(as.Date("2020/02/01"),as.Date("2020/05/10")), ylim=c(0,100))

#plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(2000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,max(max(daily_deaths),data_deaths)))
#plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/05/01"), by=1), y=rep(2000, length=74), ylim=c(0,max(max(daily_deaths[,1:75]),data_deaths)))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/11"), to=as.Date("2020/02/11")+max_days-1, by=1), y=daily_deaths[i,], col="gray", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/11"), to=as.Date("2020/02/11")+max_days-1, by=1), y=apply(daily_deaths,2,sum)/n_simuls, col="black", lwd=2)



plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(500000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,max(max(daily_infections),data_cases)))
#plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(50000, length=max_days), xlim=c(as.Date("2020/02/18"),as.Date("2020/05/01")), ylim=c(0,max(max(daily_infections[,(1:75)]),data_cases)))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/12"), to=as.Date("2020/02/12")+max_days-1, by=1), y=daily_infections[i,], col="violet", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/12"), to=as.Date("2020/02/12")+max_days-1, by=1), y=apply(daily_infections,2,sum)/n_simuls, col="darkviolet", lwd=2)
lines(x=cases_dates, y=data_cases, col="gray", lwd=2, lty="dotted")







plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(10000000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,8600000))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/12"), to=as.Date("2020/02/12")+max_days-1, by=1), y=cumul_infections[i,], col="orange", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/12"), to=as.Date("2020/02/12")+max_days-1, by=1), y=apply(cumul_infections,2,sum)/n_simuls, col="red", lwd=2)
lines(x=cases_dates, y=cumsum(data_cases), col="gray", lwd=2, lty="dotted")
#lines(x=c(as.Date("2020/04/06"), as.Date("2020/04/10")), y=c(0.035, 0.035)*inpop, col="black", lwd=3)
#lines(x=c(as.Date("2020/04/06"), as.Date("2020/04/10")), y=c(0.016, 0.016)*inpop, col="gray", lwd=1.5)
#lines(x=c(as.Date("2020/04/06"), as.Date("2020/04/10")), y=c(0.054, 0.054)*inpop, col="gray", lwd=1.5)
#lines(x=c(as.Date("2020/04/14"), as.Date("2020/04/17")), y=c(0.055, 0.055)*inpop, col="black", lwd=3)
#lines(x=c(as.Date("2020/04/14"), as.Date("2020/04/17")), y=c(0.033, 0.033)*inpop, col="gray", lwd=1.5)
#lines(x=c(as.Date("2020/04/14"), as.Date("2020/04/17")), y=c(0.077, 0.077)*inpop, col="gray", lwd=1.5)

plot(x=inhosp_date, y=data_inhosp, xlim=c(as.Date("2020/02/01"), as.Date("2020/05/10")), ylim=c(0,3000))

#plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(510000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,max(max(daily_inhospital),data_inhosp)))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/11"), to=as.Date("2020/02/11")+max_days-1, by=1), y=daily_inhospital[i,], col="green", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/11"), to=as.Date("2020/02/11")+max_days-1, by=1), y=apply(daily_inhospital,2,sum)/n_simuls, col="darkgreen", lwd=2)
lines(x=inhosp_date-1, y=data_inhosp, col="gray", lwd=2, lty="dotted")


plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(510000, length=max_days), xlim=c(as.Date("2020/02/01"),as.Date("2020/12/31")), ylim=c(0,max(max(daily_inICU),data_inICU)))

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/12"), to=as.Date("2020/02/12")+max_days-1, by=1), y=daily_inICU[i,], col="pink", lwd=0.5)
}
lines(x=seq(from=as.Date("2020/02/12"), to=as.Date("2020/02/12")+max_days-1, by=1), y=apply(daily_inICU,2,sum)/n_simuls, col="darkred", lwd=2)
lines(x=inICU_date-7, y=data_inICU, col="black", lwd=2, lty="dotted")
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(1200, length=max_days), col="red", lty="dashed", lwd=2)

overflow_prob <- sum(apply(daily_inICU[,(91:350)],1,max)>1200)



plot(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(510000, length=max_days), xlim=c(as.Date("2020/02/18"),as.Date("2020/12/31")), ylim=c(0,5))

Repr[which(is.na(Repr))] <- 0

for (i in (1:n_simuls)) {
  lines(x=seq(from=as.Date("2020/02/12"), to=as.Date("2020/02/12")+max_days-1, by=1), y=Repr[i,], col="cyan", lwd=0.5)
  
}
lines(x=seq(from=as.Date("2020/02/12"), to=as.Date("2020/02/12")+max_days-1, by=1), y=apply(Repr,2,sum)/n_simuls, col="darkblue", lwd=2)
lines(x=seq(from=as.Date("2020/02/18"), to=as.Date("2020/02/18")+max_days-1, by=1), y=rep(1, length=max_days), col="black", lty="dashed", lwd=1)


R_above_prob <- sum(apply(Repr[,(91:350)],1,max)>1)



